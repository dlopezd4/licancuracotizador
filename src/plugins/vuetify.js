import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import es from 'vuetify/es5/locale/es'

Vue.use(Vuetify, {
  iconfont: 'md',
  lang: {
    locales: { es },
    current: 'es'
  },
  theme: {
  //   primary: '#3f51b5',
     secondary: '#ff5722',
  //   accent: '#9c27b0',
  //   error: '#f44336',
  //   warning: '#ffc107',
  //   info: '#03a9f4',
  //   success: '#4caf50'
  }
})
