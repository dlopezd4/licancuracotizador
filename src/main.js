import App from './App.vue'
import Vue from 'vue'
import Vuelidate from 'vuelidate'
import './plugins/vuetify'

Vue.config.productionTip = false
Vue.use(Vuelidate)

import { store } from './store/store';

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
