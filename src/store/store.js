import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        form1: {
            valid: false,
            fecha: '',
            nroPersonas: '',
            tipoReservaPrivada: true,
            valorEntrada: 0
        },
        form2:{
            valid: false,
            desayuno: {
                incluido: false,
                yoCompro: false,
                descripcion: '',
                precioIndividual: 0
            },
            almuerzo: {
                incluido: false,
                yoCompro: false,
                descripcion: '',
                precioIndividual: 0
            },
            once: {
                incluido: false,
                yoCompro: false,
                descripcion: '',
                precioIndividual: 0
            }
        }
    },
    getters:{
        GetForm1: state => {
            return state.form1;
        }
    },
    mutations: {
        UpdateForm1: (state, payload) => {
            Vue.set(state.form1, 'valid', payload.valid);
            Vue.set(state.form1, 'fecha', payload.fecha);
            Vue.set(state.form1, 'nroPersonas', payload.nroPersonas);
            Vue.set(state.form1, 'valorEntrada', payload.valorEntrada);
            Vue.set(state.form1, 'tipoReservaPrivada', payload.tipoReservaPrivada);
        }
    },
    actions: {
        GuardarDatosPaso1: ({commit}, payload ) => {
            commit('UpdateForm1', payload);
        }
    }
});